<?php
/*-----------------------------------------------------------------------------------
|                           FrontEnd Start
------------------------------------------------------------------------------------*/
Route::namespace('FrontEnd')->group(function () {
    Route::get('/', 'FrontEndController@index')->name('home');
    Route::get('/shop', 'FrontEndController@shop')->name('shop');
    Route::get('/blog', 'FrontEndController@blog')->name('blog');
    Route::get('/contact', 'FrontEndController@contact')->name('contact');

    /*--------------------------Web Authentication Section Start---------------------*/

    //User Post
    Route::get('/post', 'AuthUserActionController@postAdd')->name('post_add');
    Route::post('/book-schema', 'AuthUserActionController@loadBookSchema')->name('book.schema');
    Route::post('/book-category', 'AuthUserActionController@loadBookCategory')->name('book.category');

    //Book control
    Route::resource('/books', 'BookController');

    //authentication after access this page
    Route::get('/my-accounts', 'UsersController@dashboard')->name('my_accounts');

    /*--------------------------Web Authentication Section End---------------------*/

    /*--------------------------Web Authentication Process start---------------------*/
    //Social Authentication
    /*Social Login start*/
    Route::get ( '/redirect/{service}', 'SocialAuthController@redirect' );
    Route::get ( '/callback/{service}', 'SocialAuthController@callback' );

    //Login Registration
    Route::get('/signin', 'FrontEndController@signin')->name('signin');
    Route::get('/signup', 'FrontEndController@signup')->name('signup');
    Route::get('/forget-password', 'FrontEndController@forgetPassword')->name('forget.password');
    Route::post('/forget-password', 'FrontEndController@checkEmail')->name('forget.password.store');
    Route::get('/reset-password', 'FrontEndController@resetPassword')->name('reset.password');
    Route::post('/reset-password', 'FrontEndController@changePassword')->name('reset.password.store');

    /*--------------------------Web Authentication Process End---------------------*/


});








/*-----------------------------------------------------------------------------------
|                           FrontEnd End
------------------------------------------------------------------------------------*/




//Route::redirect('/', '/admin/home');

Auth::routes(['register' => true]);

// Change Password Routes...
Route::get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
Route::patch('change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('permissions', 'Admin\PermissionsController');
    Route::post('permissions_mass_destroy', 'Admin\PermissionsController@massDestroy')->name('permissions.mass_destroy');
    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', 'Admin\RolesController@massDestroy')->name('roles.mass_destroy');
    Route::resource('users', 'Admin\UsersController');
    Route::post('users_mass_destroy', 'Admin\UsersController@massDestroy')->name('users.mass_destroy');
});
