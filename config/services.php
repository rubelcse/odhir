<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id' => '147618410322-oe1fnafdnas4j0vd468das27mgqo9u03.apps.googleusercontent.com',
        'client_secret' => 'qvGI5fRXh7XSBtcPCoHP-4Iq',
        'redirect' => 'http://127.0.0.1:8000/callback/google',
    ],
    'facebook' => [
        'client_id' => '1519747438194230',
        'client_secret' => '54fe767f5460a70a40d50f100e89c6e8',
        'redirect' => 'http://127.0.0.1:8000/callback/facebook',
    ],

    

];
