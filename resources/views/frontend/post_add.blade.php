@extends('frontend.layouts.master')


@section('content')
    <section class="breadcrumb-section">
        <h2 class="sr-only">Site Breadcrumb</h2>
        <div class="container">
            <div class="breadcrumb-contents">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active">Post Add</li>
                    </ol>
                </nav>
            </div>
        </div>
    </section>
    <main id="content" class="page-section inner-page-sec-padding-bottom space-db--20">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <!-- Checkout Form s-->
                    <div class="checkout-form">
                        <div class="row row-40">
                            <div class="col-12">
                                <!-- Slide Down Trigger  -->
                                <div class="checkout-quick-box mb--10 p-0">
                                </div>
                            </div>
                            <div class="col-lg-9 mb--20">
                            {!! Form::open(['method' => 'POST', 'route' => ['books.store'], 'files' => true]) !!}
                            <!-- Billing Address -->
                                <div id="billing-form" class="mb-40">
                                    <div class="row category-block mb--20" id="catgory_div">
                                        <div class="col-sm-3 col-md-4  col-12" id="parent-id-0" parent-memu-id="0">
                                            <label>Please Choose Your Book Category *</label>
                                            <select class="nice-select" id="child-of-0"
                                                    onchange="getChildCategory(this.value, this.id)">
                                                {!!  get_cat_dropdown_menus(0,1) !!}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row location-bolck">
                                        <div class="col-12 mb--20 ">
                                            <div class="block-border check-bx-wrapper">
                                                <div class="check-box">
                                                    <input type="checkbox" checked id="create_account">
                                                    <label for="create_account">Used Your Current Address?</label>
                                                </div>
                                                <div class="check-box">
                                                    <input type="checkbox" id="shiping_address" data-shipping>
                                                    <label for="shiping_address">Set Diffrent Address</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Shipping Address -->
                                    <div id="shipping-form" class="mb--40">
                                        <div class="row">
                                            <div class="col-md-6 col-12 mb--20">
                                                <label>Location*</label>
                                                <select class="nice-select">
                                                    <option value="">Please select your division</option>
                                                    <option>Dhaka</option>
                                                    <option>Rajshahi</option>
                                                    <option>Barisal</option>
                                                </select>
                                            </div>
                                            <div class="col-md-6 col-12 mb--20">
                                                <label>&nbsp;</label>
                                                <select class="nice-select">
                                                    <option value="">Please select your district</option>
                                                    <option>Bogra</option>
                                                    <option>Rajshahi</option>
                                                    <option>Bhola</option>
                                                </select>
                                            </div>
                                            <div class="col-md-12 col-12 mb--20">
                                                <input type="text"
                                                       placeholder="Apartment, suite , unit, building, floor, etc.">
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Shipping Address -->
                                    <div id="book-block" class="mb--10">
                                        <div class="row">
                                            <div class="col-12 mb--5">
                                                <div class="block-border check-bx-wrapper">
                                                    <div class="check-box box-wrapper">
                                                        <input type="checkbox" class="form-check" name="single_book" id="exampleCheck1">
                                                        <label class="form-check-label" for="exampleCheck1">Sell Single
                                                            Book?</label>
                                                    </div>
                                                    <div class="check-box box-wrapper">
                                                        <input type="checkbox" class="form-check" name="set_book" id="exampleCheck2">
                                                        <label class="form-check-label" for="exampleCheck2">Sell Full
                                                            Set?</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="book-list-wrapper">
                                            <div class="book-list-single mb--10">
                                                <div class="form-row">
                                                    <div class="col-md-1 col-12">
                                                        <br>
                                                        <span class="book-serial">
                                                        1
                                                    </span>
                                                    </div>
                                                    <div class="col-md-5 col-12">
                                                        <label>Book Name*</label>
                                                        {!! Form::text('text', old('name'), ['class' => 'mb-0 form-control','placeholder' => 'Book Name', 'id' => 'name', 'required']) !!}

                                                    </div>
                                                    <div class="col-md-2 col-12">
                                                        <label>Printing year*</label>
                                                        <select class="nice-select">
                                                            <option value="1">2018-2019</option>
                                                            <option value="2">2017-2018</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 col-12">
                                                        <label>price*</label>
                                                        <input type="number" placeholder="Enter price">
                                                    </div>
                                                    <div class="col-md-2 col-12">
                                                        <label>Discount*</label>
                                                        <select class="nice-select">
                                                            <option>10%</option>
                                                            <option>20%</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="col-md-1 col-12 mb--20">
                                                        &nbsp;
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <label>Writer Name*</label>
                                                        <select class="nice-select">
                                                            <option>Humaiyan Ahmed</option>
                                                            <option>Bumkash</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <label>Publisher Name*</label>
                                                        <select class="nice-select">
                                                            <option>G Maula Prokashoni</option>
                                                            <option>Professors</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 col-12">
                                                        <label>Condition*</label>
                                                        <select class="nice-select">
                                                            <option>New</option>
                                                            <option>old</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 col-12">
                                                        <label>Images*</label>
                                                        <input type="file" multiple placeholder="Uploads">
                                                    </div>
                                                    {{--<div class="col-md-1 col-12 mb--20">
                                                        <label>&nbsp;</label>
                                                        <p style="font-size: 11px">
                                                            <span class="badge badge-success">10</span>
                                                            <span class="text-info">Images</span>
                                                        </p>
                                                    </div>--}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mt--10 p-0 block-border"></div>
                                        <div class="row mt--10">
                                            <div class="col-md-4 col-12 mb--20">
                                                <button class="bulk-btn theme-btn" id="add-more">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                    Add More
                                                </button>
                                                <button class="bulk-btn remove-btn" id="remove-one">
                                                    <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                                    Remove One
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="info-block" class="mb--10">
                                        <div class="row mb--20">
                                            <div class="col-md-8 col-sm-12">
                                                <div class="wrapper">
                                                    <div class="wrapper-text">
                                                        <p class="m-0"><strong>Note</strong></p>
                                                        <div class="m-0 p-0 block-border"></div>
                                                        {{--<hr class="m-1">--}}
                                                        <ol class="list">
                                                            <li class="list-item">You are getting 30% taka for the
                                                                book.
                                                            </li>
                                                            <li class="list-item">Your parcel will be verified and
                                                                collected by our team form hom.
                                                            </li>
                                                            <li class="list-item">You can sell the book at no cost for
                                                                any king of tranportaion
                                                            </li>
                                                            <li class="list-item">Sell your book & get point</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="card text-black-2 text-right wrapper">
                                                    <div class="card-body">
                                                        <div class="card-text">
                                                            <ul class="list">
                                                                <li class="list-item">
                                                                    Total Book - <span
                                                                            class="badge badge-success"><strong>03</strong></span>
                                                                    pcs
                                                                </li>
                                                                <li class="list-item">Total Price - <span
                                                                            class="badge badge-success"><strong>500</strong></span>
                                                                    tk
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="submit-block">
                                        <div class="row">
                                            <div class="col-md-8 col-sm-12">
                                                <div class="submit-wrapper">
                                                    <div class="check-box mb--10">
                                                        <input type="checkbox" class="form-check" id="exampleCheck4"
                                                               name="term"
                                                               onchange="activateButton(this)">
                                                        <label class="form-check-label" for="exampleCheck4">I agree to
                                                            the
                                                            <a href="#"><strong>terms of service</strong></a></label>
                                                    </div>
                                                    <button class="btn btn--yellow btn-block" type="submit"
                                                            name="submit" id="submit">Submit Add
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

@endsection

@push('scripts')
<script>
    function activateButton(element) {
        if (element.checked) {
            document.getElementById("submit").disabled = false;
        }
        else {
            document.getElementById("submit").disabled = true;
        }
    }
    $(function () {
        //first load disable submit button
        document.getElementById("submit").disabled = true;
        //add a book into list
        $('#add-more').click(function (e) {
            var no_of_book_item = $('.book-list-single').length;
            $.ajax({
                url: "<?php echo route('book.schema') ?>",
                method: 'POST',
                data: {serial_no: no_of_book_item},
                beforeSend: function () {
                    $("#loader").show();
                },
                success: function (data) {
                    $("#book-list-wrapper").append(data.schema);
                    $('select').niceSelect();
                    $("#loader").hide();
                }
            });
        });

        //remove last book one into list
        $('#remove-one').click(function (e) {
            var no_of_book_item = $('.book-list-single').length;
            if (no_of_book_item > 1) {
                $(".book-list-single").last().remove();
            } else {
                toastr.warning('At Least One Book Submit!!');
            }
        });
    });

    // get category menus
    function getChildCategory(parent_menu_id, selectId) {
        var nexDivId = $('#parent-id-0').next('div').attr('parent-memu-id');
        var currentDivId = $('#' + selectId).parent('div').attr('id');

        if (parent_menu_id == nexDivId) return;

        $.ajax({
            url: "<?php echo route('book.category') ?>",
            method: 'POST',
            data: {parent_menu_id: parent_menu_id, isOptionOlny: 0,},
            beforeSend: function () {
                //$("#loader").show();
            },
            success: function (data) {
                var childDiv = data.catgoryData;
                $('#' + currentDivId).nextAll('div').remove();
                $("#catgory_div").append(childDiv);
                $('select').niceSelect();
                $("#loader").hide();
            }
        });
    }


</script>


@endpush