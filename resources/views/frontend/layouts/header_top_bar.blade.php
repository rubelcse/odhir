<div class="header-top header-top--style-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-8 flex-lg-right">
                <ul class="header-top-list">
                    {{-- <li class="dropdown-trigger language-dropdown"><a href=""><i
                                     class="icons-left fas fa-heart"></i>
                             wishlist (0)</a>
                     </li>--}}
                    <li class="dropdown-trigger language-dropdown"><a href="{{route('my_accounts')}}"><i
                                    class="icons-left fas fa-user"></i>
                            My Account</a><i class="fas fa-chevron-down dropdown-arrow"></i>
                        <ul class="dropdown-box">
                            <li><a href="{{route('my_accounts')}}">My Account</a></li>
                            <li><a href="">Order History</a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="btn btn-success btn-sm" id="post-add" href="{{route('post_add')}}">
                            <i class="fa fa-plus-circle mr--5" aria-hidden="true"></i>
                            Post free add
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>