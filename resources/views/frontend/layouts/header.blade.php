<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Odhir||Buy&sell Book</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Use Minified Plugins Version For Fast Page Load -->
    <link rel="stylesheet" type="text/css" media="screen"
          href="{{asset(config('viewUrl.odhirFrontEndPunlic').'/css/plugins.css')}}"/>
    <link rel="stylesheet" type="text/css" media="screen"
          href="{{asset(config('viewUrl.odhirFrontEndPunlic').'/css/main.css')}}"/>
    <link rel="stylesheet" type="text/css" media="screen"
          href="{{asset(config('viewUrl.odhirFrontEndPunlic').'/css/custome.css')}}"/>
    <link rel="shortcut icon" type="image/x-icon"
          href="{{asset(config('viewUrl.odhirFrontEndPunlic').'/image/favicon.ico')}}">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet" />


    {{--Recive dynamic style--}}
    @stack('styles')
</head>