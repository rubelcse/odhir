<!DOCTYPE html>
<html lang="zxx">

{{--Header start--}}
@include('frontend.layouts.header')
{{--Header end--}}

<body>
<div class="site-wrapper" id="top">
    {{--Loader bolock.show in every ajax request--}}
    <div id="loader">
        <p style="position: absolute; color: White; top: 50%; left: 45%;">
            <img src='{{asset('/img/loading.gif')}}'>
        </p>
    </div>


    <div class="site-header header-4 mb--20 d-none d-lg-block">
        {{--Header Top start--}}
        @include('frontend.layouts.header_top_bar')
        {{--Header Top end--}}

        {{--Header Middle bar start--}}
        @include('frontend.layouts.header_middle_bar')
        {{--Header Middle bar end--}}

        {{--Header bootom bar start--}}
        @include('frontend.layouts.header_bottom_bar')
        {{--Header bootom bar end--}}

    </div>
{{--Mobile menu start--}}
@include('frontend.layouts.mobile_header')
{{--Mobile menu end--}}

{{--Dynamic content start--}}
@yield('content')
{{--Dynamic content end--}}
<!--=================================
    Footer
===================================== -->
</div>
<!--=================================
    Brand Area
===================================== -->
@include('frontend.layouts.brand')

<!--=================================
    Footer Area
===================================== -->
@include('frontend.layouts.footer')
<!-- Use Minified Plugins Version For Fast Page Load -->
@include('frontend.layouts.footer_script')
</body>

</html>