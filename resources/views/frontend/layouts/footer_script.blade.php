<script src="{{asset(config('viewUrl.odhirFrontEndPunlic').'/js/plugins.js')}}"></script>
<script src="{{asset(config('viewUrl.odhirFrontEndPunlic').'/js/ajax-mail.js')}}"></script>
<script src="{{asset(config('viewUrl.odhirFrontEndPunlic').'/js/custom.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script type="text/javascript">
    /*Every ajax request header with csrf protection added*/
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    /*Toaster notification*/
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

     @if(\Session::has('message'))
        var type = "{{ \Session::get('alert-type', 'info') }}";
        switch (type) {
            case 'info':
                toastr.info("{{ \Session::get('message') }}");
                break;

            case 'warning':
                toastr.warning("{{ \Session::get('message') }}");
                break;

            case 'success':
                toastr.success("{{ \Session::get('message') }}");
                break;

            case 'error':
                toastr.error("{{ \Session::get('message') }}");
                break;
        }
    @endif
</script>

{{--Recive dymanamic scripts--}}
@stack('scripts')