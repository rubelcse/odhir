<div class="header-bottom">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-3">
                <nav class="category-nav  primary-nav @yield('show-category')">
                    <div>
                        <a href="javascript:void(0)" class="category-trigger"><i
                                    class="fa fa-bars"></i>Browse
                            categories</a>
                        <ul class="category-menu">
                            {!!  get_menus() !!}

                            {{--<li class="cat-item has-children">
                                <a href="#">{{ app('request')->input('a') }}</a>
                                <ul class="sub-menu">
                                    <li><a href="#">Bags & Cases</a></li>
                                    <li><a href="#">Binoculars & Scopes</a></li>
                                    <li><a href="#">Digital Cameras</a></li>
                                    <li><a href="#">Film Photography</a></li>
                                    <li><a href="#">Lighting & Studio</a></li>
                                </ul>
                            </li>--}}
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="col-lg-3">
                <div class="header-phone ">
                    <div class="icon">
                        <i class="fas fa-headphones-alt"></i>
                    </div>
                    <div class="text">
                        <p>Free Support 24/7</p>
                        <p class="font-weight-bold number">+01-202-555-0181</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="main-navigation flex-lg-right">
                    <ul class="main-menu menu-right li-last-0">
                        <li class="menu-item has-children">
                            <a href="{{route('home')}}">Home</a>
                        </li>
                        <!-- Shop -->
                        <li class="menu-item has-children mega-menu">
                            <a href="{{route('shop')}}">Shop</a>
                        </li>
                        <!-- Blog -->
                        <li class="menu-item has-children mega-menu">
                            <a href="{{route('blog')}}">Blog</a>
                        </li>
                        <li class="menu-item">
                            <a href="{{route('contact')}}">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>