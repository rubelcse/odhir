<div class="book-list-single">
    <div class="mb--5 mt--10 p-0 block-border"></div>
    <div class="form-row">
        <div class="col-md-1 col-12">
            <br>
            <span class="book-serial">
                {{++$serial_no}}
            </span>
        </div>
        <div class="col-md-5 col-12">
            <label>Book Name*</label>
            <input type="text" placeholder="Book Name">
        </div>
        <div class="col-md-2 col-12">
            <label>Printing year*</label>
            <select class="nice-select">
                <option>2018-2019</option>
                <option>2017-2018</option>
            </select>
        </div>
        <div class="col-md-2 col-12">
            <label>price*</label>
            <input type="number" placeholder="Enter price">
        </div>
        <div class="col-md-2 col-12">
            <label>Discount*</label>
            <select class="nice-select">
                <option>10%</option>
                <option>20%</option>
            </select>
        </div>
    </div>
    <div class="form-row">
        <div class="col-md-1 col-12">
            &nbsp;
        </div>
        <div class="col-md-3 col-12">
            <label>Writer Name*</label>
            <select class="nice-select">
                <option>Humaiyan Ahmed</option>
                <option>Bumkash</option>
            </select>
        </div>
        <div class="col-md-3 col-12">
            <label>Publisher Name*</label>
            <select class="nice-select">
                <option>G Maula Prokashoni</option>
                <option>Professors</option>
            </select>
        </div>
        <div class="col-md-2 col-12">
            <label>Condition*</label>
            <select class="nice-select">
                <option>New</option>
                <option>old</option>
            </select>
        </div>
        <div class="col-md-3 col-12">
            <label>Images*</label>
            <input type="file" multiple placeholder="Uploads">
        </div>
        {{--<div class="col-md-1 col-12 mb--20">
            <label>&nbsp;</label>
            <p style="font-size: 11px">
                <span class="badge badge-success">10</span>
                <span class="text-info">Images</span>
            </p>
        </div>--}}
    </div>
</div>