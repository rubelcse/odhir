@extends('frontend.layouts.master')


@section('content')
    <section class="breadcrumb-section">
        <h2 class="sr-only">Site Breadcrumb</h2>
        <div class="container">
            <div class="breadcrumb-contents">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item active">Login</li>
                    </ol>
                </nav>
            </div>
        </div>
    </section>
    <!--=============================================
   =            Login Register page content         =
   =============================================-->
    <main class="page-section inner-page-sec-padding-bottom">
        <div class="container">
            @include('.message')
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12">
                    {!! Form::open(['method' => 'POST', 'route' => ['login']]) !!}
                    <div class="login-form">
                            <h4 class="login-title">Already You Have an Account?</h4>
                            <p><span class="font-weight-bold">Sign in now</span></p>
                            <div class="row">
                                <div class="col-md-12 col-12 mb--15">
                                    {!! Form::label('email', 'Email') !!} <span class="mandatory">*</span>
                                    {!! Form::text('email', old('email'), ['class' => 'mb-0 form-control', 'placeholder' => 'Enter Email', 'id' => 'email', 'required']) !!}
                                </div>
                                <div class="col-12 mb--20">
                                    {!! Form::label('password', 'Password', ['class' => 'control-label']) !!} <span class="mandatory">*</span>
                                    {!! Form::text('password', old('password'), ['class' => 'mb-0 form-control', 'placeholder' => 'Enter Password', 'id' => 'password', 'required']) !!}
                                    <p class="help-block"></p>
                                    <a href="" class="forget-pass">Forgotten Password</a>
                                </div>
                                <div class="col-md-12">
                                    <input type="submit" class="btn btn-outlined" value="Login">
                                </div>
                                <div class="col-md-12 mt--10">
                                    <a href="{{url('/redirect/facebook')}}" class="loginBtn loginBtn--facebook">
                                        Login with Facebook
                                    </a>

                                    <a href="{{url('/redirect/google')}}" class="loginBtn loginBtn--google">
                                        Login with Google
                                    </a>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-sm-12 col-md-12 col-xs-12 col-lg-6 mb--30 mb-lg--0">
                    <!-- Login Form s-->
                    <div class="login-box">
                        <h4 class="login-title">New to {{config('app.name')}}?</h4>
                        <p><span class="font-weight-bold">Sign up now, it’s quick & free</span></p>
                        <p> By creating an account you will be able to shop faster, be up to date on an order's
                            status, and keep track of the
                            orders
                            you have previously made.</p>
                        <a href="{{route('signup')}}" class="btn btn-outlined   mr-3">Sign Up</a>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@push('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2D8wrWMY3XZnuHO6C31uq90JiuaFzGws"></script>
@endpush