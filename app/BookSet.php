<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookSet extends Model
{
    protected $fillable = ['name', 'slug'];
}
