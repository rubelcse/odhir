<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function dashboard()
    {
        return view($this->frontView.'auth.dashboard');
    }
}
