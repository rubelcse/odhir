<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Symfony\Component\Console\Helper\Table;

class MenuController extends Controller
{
    public function index()
    {
        $data = json_decode(DB::table('menus')
            ->where([['status', '=', '0'], ['parent_id', '=', '0']])
            ->get()
            ->toJson(), true);
        $data = $this->get_menus();
        echo ($data);
        die();
        return view($this->frontView . 'home');
    }

    public function get_menus()
    {
        $menus = '';
        $menus .= $this->get_multilevel_menus(0,0);
        $menus=str_replace('<ul class="sub-menu"></ul>','',$menus);
        return $menus;
    }

    public function get_multilevel_menus($parent_id=0,$root_menu=0)
    {
        $menu = '';
        $data = json_decode(DB::table('menus')
           ->where([['status', '=', '1'], ['parent_id', '=', $parent_id]])
            ->orderBy('sort_id', 'asc')->get()->toJson(), true);
        foreach($data as $k=>$val){
                if ($val['has_child']==0) {
                    $menu.='<li class="cat-item "><a href="'. $val['slug'] .'">' . $val['menu_name'].'</a>';
                } else {
                    $menu .= '<li class="cat-item has-children"><a href="'. $val['slug'] .'">' . $val['menu_name'].'</a>';
                }
           if($root_menu!=1) $menu.='<ul class="sub-menu">'. $this->get_multilevel_menus($val['id']). '</ul>';
            $menu.='</li>';
        }

        return $menu;
    }


}
