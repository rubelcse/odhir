<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthUserActionController extends Controller
{
    public function postAdd()
    {
        return view($this->frontView.'post_add');
    }

    public function loadBookSchema(Request $request)
    {
        $serial_no = $request->serial_no;
        $data = view($this->frontView.'.schema',compact('serial_no'))->render();
        return response()->json(['schema'=>$data]);
    }

    public function loadBookCategory(Request $request)
    {
        $menu_id = $request->parent_menu_id;
        $isOptionOlny = $request->isOptionOlny;
        $categoryData=get_cat_dropdown_menus($menu_id,$isOptionOlny);
        return response()->json(['catgoryData'=>$categoryData]);
    }

}
