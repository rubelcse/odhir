<?php

namespace App\Http\Controllers\FrontEnd;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;
use Mail;

class SocialAuthController extends Controller
{
    //Dynamic social service
    public function redirect($service)
    {
        return Socialite::driver($service)->redirect();
    }

    public function callback($service)
    {
        $socialUser = Socialite::with($service)->user();
        $socialUserEmail = $socialUser->email;
        //Email wise user found or not
        $user = \App\User::where(['email' => $socialUserEmail])->first();
        if (!empty($user)) {
            //matching existing user
            if (($user->provider_id != $socialUser->id) && ($user->provider_name != $service)) {
                $user->provider_id = $socialUser->id;
                $user->provider_name = $service;
                $user->save();
            }
        } else {
            $user = \App\User::where(['provider_id' => $socialUser->id, 'provider_name' => $service])->first();
            if(empty($user)) {
                //create user
                $v_password=mt_rand(100000, 999999);
                $data['name'] = !empty($socialUser->name) ? $socialUser->name : $socialUserEmail;
                $data['email'] = $socialUserEmail;
                $data['id_card'] = time() . '-' . rand(1, 100);
                $data['password'] = bcrypt($v_password);
                $data['user_level'] = 5;
                $data['is_verified'] = 1;
                $data['provider_id'] = $socialUser->id;
                $data['provider_name'] = $service;
                $data['verify_token'] = null;
                $user = User::create($data);
               /* if (!empty($user->email)) {
                    Mail::send('emails.social_signup_password_notify', ['email' => $user->email, 'password' =>  $v_password], function ($m) use ($user) {
                        $setting = Setting::first();
                        $m->to($user->email, $user->name)->from($setting->contact_email, $setting->organization_name)->subject('Registration complete from Nibedon!');
                    });
                }*/
            }

        }

        if(\Auth::loginUsingId($user->id)){
            return Redirect::to(route('home'));
        }
    }
}
