<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FrontEndController extends Controller
{
    public function index()
    {
        return view($this->frontView.'home');
    }

    public function shop()
    {
        return view($this->frontView.'shop');
    }

    public function blog()
    {
        return view($this->frontView.'blog');
    }

    public function contact()
    {
        return view($this->frontView.'contact');
    }

    public function signin()
    {
        Auth::logout();
        return view($this->frontView.'.signin');
    }

    public function signup()
    {
        return view($this->frontView.'signup');
    }

}
