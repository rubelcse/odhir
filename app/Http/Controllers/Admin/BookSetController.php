<?php

namespace App\Http\Controllers\Admin;

use App\BookSet;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BookSetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BookSet  $bookSet
     * @return \Illuminate\Http\Response
     */
    public function show(BookSet $bookSet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BookSet  $bookSet
     * @return \Illuminate\Http\Response
     */
    public function edit(BookSet $bookSet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BookSet  $bookSet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BookSet $bookSet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BookSet  $bookSet
     * @return \Illuminate\Http\Response
     */
    public function destroy(BookSet $bookSet)
    {
        //
    }
}
