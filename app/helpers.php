<?php
/**
 * Created by PhpStorm.
 * User: mdrub
 * Date: 12/16/2019
 * Time: 1:09 PM
 */
function camelCaseToSlug($input = null, $symbol = '-')
{
    $input = is_null( $input ) ? baseRoute() : $input;

    return strtolower( preg_replace( '/(?<!^)[A-Z]/', $symbol . '$0', $input ) );
}


function baseRoute($route = null)
{
    $route = $route ? $route : request()->route() ? request()->route()->getName() : null;
    //dd($route);
    return substr( $route, 0, strpos( $route, '.' ) );
}

/*This two function is used to generate dynamic multilyer menus*/
 function get_menus()
{
    $menus = '';
    $menus .= get_multilevel_menus(0,0);
    $menus=str_replace('<ul class="sub-menu"></ul>','',$menus);
    return $menus;
}

 function get_multilevel_menus($parent_id=0,$root_menu=0)
{
    $menu = '';
    $data = json_decode(DB::table('menus')
        ->where([['status', '=', '1'], ['parent_id', '=', $parent_id]])
        ->orderBy('sort_id', 'asc')->get()->toJson(), true);
    foreach($data as $k=>$val){
        if ($val['has_child']==0) {
            $menu.='<li class="cat-item"><a href="'. $val['slug'] .'">' . $val['menu_name'].'</a>';
        } else {
            $menu .= '<li class="cat-item has-children"><a href="'. $val['slug'] .'">' . $val['menu_name'].'</a>';
        }
        if($root_menu!=1) $menu.='<ul class="sub-menu">'. get_multilevel_menus($val['id']). '</ul>';
        $menu.='</li>';
    }

    return $menu;
}
/*End function is used to generate dynamic multilyer menus*/


/*This  function is used to generate dynamic category menus*/
function get_cat_dropdown_menus($parent_id=0,$isOptionOnly=0)
{
    $data = json_decode(DB::table('menus')
        ->where([['status', '=', '1'], ['parent_id', '=', $parent_id]])
        ->orderBy('sort_id', 'asc')->get()->toJson(), true);

    $categoryDivData='';
    if(sizeof($data)>0){
        $categoryDivData.= '<div class="col-sm-3 col-md-4  col-12" id="parent-id-'.$parent_id.'" parent-memu-id="'.$parent_id.'"><label>&nbsp; </label>';
        $categoryDivData.= ' <select class="nice-select " id="child-of-'.$parent_id.'" onchange="getChildCategory(this.value, this.id)">';
        $dropDownOptions = '<option value=""  > -- Select Option --</option>';

        foreach($data as $k=>$val) {
            $dropDownOptions .= '<option value="'.$val['id'].'">' . $val['menu_name'] . '</option>';
        }
        $categoryDivData.=$dropDownOptions;
        $categoryDivData.='</select></div>';
    }

    if($isOptionOnly==1) return $dropDownOptions;

    return $categoryDivData;
}
/*End dynamic category menus*/