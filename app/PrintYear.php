<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrintYear extends Model
{
    protected $fillable = ['from','to'];
}
